# Bleachbit as root

Run bleachbit as root for cleaning system files. Patched version for run by menu, search and rofi.

Usage: `bleachbit-root`

## INSTALLATION

### From the [AUR](https://aur.archlinux.org/packages/bleachbit-admin/):
```
git clone https://aur.archlinux.org/bleachbit-admin.git
cd bleachbit-admin/
makepkg -sci
```

### Install from source:
Unpack the source package and run command (like root):
```
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build own ArchLinux package
You need these packages for building the ArchLinux package:
```
base-devel git wget yajl bleachbit polkit
```

Run command:
```
make build-arch
```

### Install from *.pkg.tar.xz package:
Run command (like root):
```
pacman -U distrib/bleachbit-admin*.pkg.tar.xz
```
